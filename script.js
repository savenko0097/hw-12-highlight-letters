'use strict';


let lastBtn = null;

function changeColor(id) {
  let btn = document.getElementById(id);

  if (btn.classList.contains("blue")) {
    btn.classList.remove("blue");
    btn.classList.add("btn");
  } else {
    btn.classList.remove("btn");
    btn.classList.add("blue");
  }

  if (lastBtn !== null && lastBtn !== btn) {
    lastBtn.classList.remove("blue");
    lastBtn.classList.add("btn");
  }

  lastBtn = btn;
}

document.addEventListener("keydown", function (event) {
  if (event.code === "KeyE") {
    changeColor("btn-e");
  } else if (event.code === "KeyN") {
    changeColor("btn-n");
  } else if (event.code === "KeyS") {
    changeColor("btn-s");
  } else if (event.code === "KeyO") {
    changeColor("btn-o");
  } else if (event.code === "KeyL") {
    changeColor("btn-l");
  } else if (event.code === "KeyZ") {
    changeColor("btn-z");
  } else if (event.code === "Enter") {
    changeColor("btn-enter");
  }
});

